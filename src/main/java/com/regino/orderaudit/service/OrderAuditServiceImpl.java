package com.regino.orderaudit.service;

import com.regino.orderaudit.listener.preview.OrderPreview;
import com.regino.orderaudit.converter.OrderAuditConverter;
import com.regino.orderaudit.dto.OrderAuditDto;
import com.regino.orderaudit.entity.OrderAuditEntity;
import com.regino.orderaudit.repository.OrderAuditRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class OrderAuditServiceImpl implements OrderAuditService {

    private final OrderAuditRepository orderAuditRepository;
    private final OrderAuditConverter orderAuditConverter;

    @Override
    public Collection<OrderAuditDto> findAll() {
        return orderAuditRepository.findAll().stream()
                .map(orderAuditConverter::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public OrderAuditDto createRecord(OrderPreview order) {
        OrderAuditDto savedRecord = saveRecord(order);
        log.info("created new record with id: {}", savedRecord.getId());
        return savedRecord;
    }

    @Override
    public OrderAuditDto editRecord(OrderPreview order) {
        if (!orderAuditRepository.existsById(order.getId())) {
            log.info("trying to update nonexistent record, id: {}", order.getId());
            throw new IllegalStateException("record doesn't exist");
        }
        OrderAuditDto updatedOrder = saveRecord(order);
        log.info("order with id: {} updated", updatedOrder.getId());
        return updatedOrder;
    }

    private OrderAuditDto saveRecord(OrderPreview order) {
        OrderAuditEntity entityToUpdate = orderAuditConverter.toEntity(order);
        OrderAuditEntity updatedEntity = orderAuditRepository.save(entityToUpdate);
        return orderAuditConverter.toModel(updatedEntity);
    }

    @Override
    public void deleteRecord(long orderId) {
         orderAuditRepository.markAsDeleted(orderId);
         log.info("order #{} marked as deleted", orderId);
    }
}
