package com.regino.orderaudit.service;

import com.regino.orderaudit.listener.preview.OrderPreview;
import com.regino.orderaudit.dto.OrderAuditDto;

import java.util.Collection;

public interface OrderAuditService {
    Collection<OrderAuditDto> findAll();

    OrderAuditDto createRecord(OrderPreview orderAuditDto);

    OrderAuditDto editRecord(OrderPreview orderAuditDto);

    void deleteRecord(long orderId);
}
