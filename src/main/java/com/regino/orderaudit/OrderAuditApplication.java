package com.regino.orderaudit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderAuditApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderAuditApplication.class, args);
	}

}
