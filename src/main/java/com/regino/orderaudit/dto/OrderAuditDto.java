package com.regino.orderaudit.dto;

import com.regino.orderaudit.entity.Currency;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
public class OrderAuditDto {
    private final Long id;
    private final String total;
    private final Currency currency;
    private final boolean isDeleted;
    private final LocalDateTime createDate;
    private final LocalDateTime lastEditDate;
}
