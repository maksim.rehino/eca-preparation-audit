package com.regino.orderaudit.listener.preview;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.regino.orderaudit.entity.Currency;
import lombok.*;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(NON_NULL)
public class OrderPreview {
    private final Long id;
    private final String total;
    private final Currency currency;
}
