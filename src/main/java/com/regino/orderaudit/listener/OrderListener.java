package com.regino.orderaudit.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.regino.orderaudit.config.RabbitMqConfig;
import com.regino.orderaudit.listener.preview.OrderPreview;
import com.regino.orderaudit.service.OrderAuditService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Slf4j
@Component
public class OrderListener {
    private final OrderAuditService orderAuditService;
    private final ObjectMapper mapper;

    @SneakyThrows
    @RabbitListener(queues = RabbitMqConfig.NEW_ORDERS_QUEUE)
    public void newOrdersListener(String orderAsString) {
        OrderPreview orderPreview = mapper.readValue(orderAsString, OrderPreview.class);
        log.info("received order from queue: {}", orderPreview);
        orderAuditService.createRecord(orderPreview);
    }

    @RabbitListener(queues = RabbitMqConfig.DELETE_ORDER_QUEUE)
    public void ordersToDeleteListener(long idToDelete) {
        log.info("recieved id: {} to delete", idToDelete);
        orderAuditService.deleteRecord(idToDelete);
    }
}
