package com.regino.orderaudit.converter;

import com.regino.orderaudit.listener.preview.OrderPreview;
import com.regino.orderaudit.dto.OrderAuditDto;
import com.regino.orderaudit.entity.OrderAuditEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Component
public class OrderAuditConverter {

    public OrderAuditEntity toEntity(OrderAuditDto orderDto) {
        return new OrderAuditEntity(
                orderDto.getId(),
                new BigDecimal(orderDto.getTotal()),
                orderDto.getCurrency(),
                orderDto.isDeleted(),
                orderDto.getCreateDate(),
                LocalDateTime.now()
        );
    }

    public OrderAuditEntity toEntity(OrderPreview preview) {
        return OrderAuditEntity.builder()
                .id(preview.getId())
                .total(new BigDecimal(preview.getTotal()))
                .currency(preview.getCurrency())
                .createdAt(LocalDateTime.now())
                .changedAt(LocalDateTime.now())
                .build();
    }

    public OrderAuditDto toModel(OrderAuditEntity savedEntity) {
        return new OrderAuditDto(
                savedEntity.getId(),
                savedEntity.getTotal().setScale(2, RoundingMode.HALF_UP).toString(),
                savedEntity.getCurrency(),
                savedEntity.isDeleted(),
                savedEntity.getCreatedAt(),
                savedEntity.getChangedAt()
        );
    }
}
