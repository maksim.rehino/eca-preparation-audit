package com.regino.orderaudit.controller;

import com.regino.orderaudit.dto.OrderAuditDto;
import com.regino.orderaudit.service.OrderAuditService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderAuditService orderAuditService;

    @GetMapping
    public Collection<OrderAuditDto> findAll() {
        return orderAuditService.findAll();
    }
}
