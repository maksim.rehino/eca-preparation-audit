package com.regino.orderaudit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    public static final String EXCHANGE_NAME = "order-exchange";

    public static final String NEW_ORDERS_QUEUE = "orders";
    public static final String DELETE_ORDER_QUEUE = "orders-delete";

    @Bean
    Queue queue() {
        return new Queue(NEW_ORDERS_QUEUE, false);
    }

    @Bean
    Queue queue2() {
        return new Queue(DELETE_ORDER_QUEUE, false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(EXCHANGE_NAME);
    }

    @Bean
    Binding binding(DirectExchange exchange) {
        return BindingBuilder.bind(queue()).to(exchange).with("new-orders");
    }

    @Bean
    Binding binding2(DirectExchange exchange) {
        return BindingBuilder.bind(queue2()).to(exchange).with("orders-to-delete");
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(NEW_ORDERS_QUEUE, DELETE_ORDER_QUEUE);
        return container;
    }
}
