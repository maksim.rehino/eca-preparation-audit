package com.regino.orderaudit.repository;

import com.regino.orderaudit.entity.OrderAuditEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface OrderAuditRepository extends JpaRepository<OrderAuditEntity, Long> {

    @Transactional
    @Modifying
    @Query("update OrderAuditEntity oa set oa.isDeleted = true where oa.id = :orderId")
    void markAsDeleted(long orderId);
}
