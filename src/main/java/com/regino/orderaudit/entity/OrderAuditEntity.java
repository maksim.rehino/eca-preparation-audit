package com.regino.orderaudit.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static javax.persistence.EnumType.STRING;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "audit_orders")
public class OrderAuditEntity {

    @Id
    private Long id;

    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "currency")
    @Enumerated(STRING)
    private Currency currency;

    @Column(name = "isDeleted")
    private boolean isDeleted = false;

    @Column(name = "createdAt")
    @CreatedDate
    private LocalDateTime createdAt;

    @Column(name = "changedAt")
    @LastModifiedDate
    private LocalDateTime changedAt;
}
